import models.Customer;
import models.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(0, "an", 50);
        Customer customer2 = new Customer(1, "minh", 10);
        System.out.println(customer1);
        System.out.println(customer2);

        Invoice invoice1 = new Invoice(0, customer1, 10000);
        Invoice invoice2 = new Invoice(1, customer2, 5000);
        System.out.println(invoice1);
        System.out.println(invoice2);

    }
}
